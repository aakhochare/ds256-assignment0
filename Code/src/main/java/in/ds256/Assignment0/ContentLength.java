package in.ds256.Assignment0;


import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
/**
 * DS-256 Assignment 0
 * Code for question 4(URLs with the largest and smallest content. What is the average page length?)
 */
public class ContentLength 
{
    public static void main( String[] args )
    {
	SparkConf conf = new SparkConf().setAppName("ContentLength");
        JavaSparkContext sc = new JavaSparkContext(conf);
        String path = args[0];

	/**
	 * Code goes here
	 */

        sc.stop();
    }
}
