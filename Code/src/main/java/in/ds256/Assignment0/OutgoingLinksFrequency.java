package in.ds256.Assignment0;


import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
/**
 * DS-256 Assignment 0
 * Code for question 3(Frequency distribution for the number of outgoing (non-distinct) links from each webpage of Content-Type: text/html goes here)
 */
public class OutgoingLinksFrequency 
{
    public static void main( String[] args )
    {
	SparkConf conf = new SparkConf().setAppName("OutgoingLinksFrequency");
        JavaSparkContext sc = new JavaSparkContext(conf);
        String path = args[0];

	/**
	 * Code goes here
	 */

        sc.stop();
    }
}
