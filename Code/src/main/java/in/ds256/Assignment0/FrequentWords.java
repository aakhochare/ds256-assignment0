package in.ds256.Assignment0;


import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
/**
 * DS-256 Assignment 0
 * Code for question 2(Histogram of top 100 most frequent words, while ignoring stop words) goes here
 */
public class FrequentWords 
{
    public static void main( String[] args )
    {
	SparkConf conf = new SparkConf().setAppName("FrequentWords");
        JavaSparkContext sc = new JavaSparkContext(conf);
        String path = args[0];

	/**
	 * Code goes here
	 */

        sc.stop();
    }
}
